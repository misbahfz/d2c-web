export default {
    formBuilderData(state) {
        return state.formBuilderData
    },
    formBuilderList(state) {
        return state.formBuilderList
    },
}
