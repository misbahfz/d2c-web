const apiUrl = process.env.VUE_APP_API_URL
import axios from 'axios'
const headers = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('vueauth_token')}`,
    },
}
export default {
    async getFormBuilderList({ commit }, payLoad) {
        commit('setLoading', true)
        try {
            const { data } = await axios.get(
                apiUrl + 'form-builder/',
                {
                    params: {
                        dataSegmentId: payLoad.dataSegmentId,
                        section: payLoad.section,
                    },
                },
                headers
            )
            commit('setFormBuilderList', JSON.parse(JSON.stringify(data.data)))
            commit('setLoading', false)
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors)
        }
    },
}
