export default {
    setFormBuilderData(state, data) {
        if (data) {
            state.formBuilderData = data
        }
    },
    setFormBuilderList(state, data) {
        if (data) {
            state.formBuilderList = data
        }
    },
}
