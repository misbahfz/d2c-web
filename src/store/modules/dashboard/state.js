export default {
    alertsFilteredData: [],
    chartsHeadings: {
        activityCharts: {
            'activity-timeline-report': {
                header: ['Approved', 'Rejected', 'On boarded', 'Date'],
                mainHeader: ['Activity Timeline'],
            },
            'screening-by-type': {
                mainHeader: ['KYC Alerts by Screening Type'],
                dataHeaders: [
                    {
                        header: ['Name Screening Hit'],
                        subHeader: {
                            group: 'Sanction hit',
                            others: 'PEP, Enforcement or Black list hit',
                        },
                    },
                    {
                        header: ['Document Verification Hit'],
                        subHeader: {
                            group: 'Documents nearing expiry',
                            others: 'Documents mismatched',
                        },
                    },
                    {
                        header: ['Risk rating hit'],
                        subHeader: {
                            group: 'Periodic Review due',
                            others: 'High Risk or Sanctioned',
                        },
                    },
                ],
            },
            'alerts-by-type': {
                mainHeader: ['KYC Alerts by Alert Type'],
                dataHeaders: [
                    {
                        header: ['On boarding'],
                        subHeader: {
                            count: 'No. of Alerts',
                            percentage: 'Percentage',
                        },
                    },
                    {
                        header: ['Triggered'],
                        subHeader: {
                            count: 'No. of Alerts',
                            percentage: 'Percentage',
                        },
                    },
                    {
                        header: ['Periodic'],
                        subHeader: {
                            count: 'No. of Alerts',
                            percentage: 'Percentage',
                        },
                    },
                    {
                        header: ['Manual'],
                        subHeader: {
                            count: 'No. of Alerts',
                            percentage: 'Percentage',
                        },
                    },
                ],
            },
            'screen-hit-by-type': {
                mainHeader: ['Name Screening Hit by Type'],
                dataHeaders: [
                    {
                        header: ['Enforcement Hit'],
                        subHeader: {
                            count: 'No. of Hits',
                            percentage: 'Percentage',
                        },
                    },
                    {
                        header: ['Client Black List Hit'],
                        subHeader: {
                            count: 'No. of Hits',
                            percentage: 'Percentage',
                        },
                    },
                    {
                        header: ['PEP Hit'],
                        subHeader: {
                            count: 'No. of Hits',
                            percentage: 'Percentage',
                        },
                    },
                    {
                        header: ['Sanction Hit'],
                        subHeader: {
                            count: 'No. of Hits',
                            percentage: 'Percentage',
                        },
                    },
                ],
            },
            'on-board-issue': {
                header: [
                    'No. of Customers With Alert',
                    'No. of Customers Without Alert',
                ],
                mainHeader: ['Customer Onboarding With / Without Alert'],
            },
            'aging-by-alert-type': {
                header: {
                    group: 'Range',
                    manual: 'Manual',
                    on_boarding: 'Onboarding',
                    periodic: 'Periodic',
                    triggered: 'Triggered',
                },
                mainHeader: ['Aging by Alert Type'],
            },
        },
        statsCharts: {
            'customer-by-age': {
                header: ['Count', 'Age Group', 'Percentage'],
                mainHeader: ['Customer On boarded'],
            },
            'customers-by-gender': {
                header: ['Count', 'Gender Name', 'Percentage'],
                mainHeader: ['Customers by Gender Group'],
            },
            'customers-by-work-type': {
                header: ['No. of Customers', 'Work type', 'Percentage'],
                mainHeader: ['Customers by Work Type'],
            },
            'customers-account-by-month': {
                header: ['No. of Opened Accounts', 'Months', 'Percentage'],
                mainHeader: ['Customers Account Opening by Month'],
            },
            'customers-by-industry': {
                header: ['No. of Customers', 'Industry type', 'Percentage'],
                mainHeader: ['Customers by Industry'],
            },
            'customers-by-nationality': {
                header: ['No. of Customers', 'Nationality', 'Percentage'],
                mainHeader: ['Customers by Nationality'],
            },
            'customers-by-residence': {
                header: [
                    'No. of Customers',
                    'Country of Residence',
                    'Percentage',
                ],
                mainHeader: ['Customers by Country of Residence'],
            },
        },
    },
}
