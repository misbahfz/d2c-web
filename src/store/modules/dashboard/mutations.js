export default {
    setAlertsFilteredData(state, data) {
        localStorage.setItem('alertStoreData', JSON.stringify(data))
        state.alertsFilteredData = data
    },
}
