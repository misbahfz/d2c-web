import axios from 'axios'
const apiUrl = process.env.VUE_APP_API_URL
const headers = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('vueauth_token')}`,
    },
}

export default {
    async initiateReview({ commit }, payload) {
        commit('setLoading', true)
        try {
            let response = await axios.put(
                apiUrl + 'alerts/initiate-review/' + payload.id,
                payload,
                headers
            )
            commit('setSuccess', 'Review has been initiated successfully')
            commit('reloadList', true)
            console.log(response)
        } catch ({ response }) {
            commit('setLoading', false)
            let { data } = response
            let message = 'There is someting wrong'
            if (data.errors && Object.keys(data.errors).length) {
                message = data.errors[Object.keys(data.errors)[0]]
            } else if (data.message) {
                message = data.message
            }
            commit('setError', message)
        }
    },
    async kycStatusReview({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.put(
                apiUrl + 'alerts/kyc-status-review/' + payload.id,
                payload,
                headers
            )
            localStorage.removeItem('alertRecord')
            localStorage.setItem('alertRecord', JSON.stringify(data.data))
            commit('setSuccess', 'Review has been submitted successfully')
            commit('reloadList', true)
        } catch ({ response }) {
            commit('setLoading', false)
            let { data } = response
            let message = 'There is someting wrong'
            if (data.errors && Object.keys(data.errors).length) {
                message = data.errors[Object.keys(data.errors)[0]]
            } else if (data.message) {
                message = data.message
            }
            commit('setError', message)
        }
    },
    async openManualAlert({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.post(
                apiUrl + 'alerts/',
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
        } catch ({ response }) {
            commit('setLoading', false)
            let { data } = response
            let message = 'There is someting wrong'
            if (data.errors && Object.keys(data.errors).length) {
                message = data.errors[Object.keys(data.errors)[0]]
            } else if (data.message) {
                message = data.message
            }
            commit('setError', message)
        }
    },
    async cancelReview({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.put(
                apiUrl + 'alerts/cancel-review/' + payload.id,
                payload,
                headers
            )
            localStorage.removeItem('alertRecord')
            localStorage.setItem('alertRecord', JSON.stringify(data.data))
            commit('setSuccess', 'Review has been cancelled successfully')
            commit('reloadList', true)
        } catch ({ response }) {
            commit('setLoading', false)
            let { data } = response
            let message = 'There is someting wrong'
            if (data.errors && Object.keys(data.errors).length) {
                message = data.errors[Object.keys(data.errors)[0]]
            } else if (data.message) {
                message = data.message
            }
            commit('setError', message)
        }
    },
}
