export default {
    // System user status options
    filterByStatusOption: [
        { text: 'Filter by status', value: null },
        { text: 'Active', value: 'active' },
        { text: 'Pending', value: 'pending' },
        { text: 'Archived', value: 'archived' },
    ],

    // System user table fields
    systemUsersfields: [
        {
            key: 'profile',
            label: '',
            class: 'profile-absolute',
        },
        {
            key: 'firstName',
            label: 'Full Name',
            sortable: true,
            formatter: (value, key, item) => {
                return `${item.firstName} ${item.lastName}`
            },
        },
        {
            key: 'role.title',
            label: 'Role',
            sortable: true,
        },
        {
            key: 'data_segments',
            label: 'DATA SEGMENTS',
            sortable: true,
        },
        {
            key: 'createdAt',
            label: 'Join Date',
            sortable: true,
        },
        {
            key: 'status',
            label: 'Status',
            class: 'text-center status-absolute-pos',
            sortable: true,
        },
        {
            key: 'action',
            label: 'Action',
            sortable: false,
            class: 'text-center w-180px',
        },
    ],
    usersList: [],
}
