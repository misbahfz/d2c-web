export default {
    filterByStatusOption: (state) => state.filterByStatusOption,
    systemUsersfields: (state) => state.systemUsersfields,
    usersList: (state) => state.usersList,
}
