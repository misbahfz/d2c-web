export default {
    state: {
        loading: false,
        error: null,
        selectedItem: null,
        success: null,
        redirectTo: null,
        reload: false,
        data: [],
        hidePopup: false,
        showPageLoader: false,
        onSubmit: {},
        redirectUrl: null,
        kycRole: '',
        changeDetect: false,
        displaySettings: localStorage.getItem('displaySettings')
            ? JSON.parse(localStorage.getItem('displaySettings'))
            : null,
        fileLoading: false,
        tokenExpiryTime: localStorage.getItem('tokenExpiryTime')
            ? JSON.parse(localStorage.getItem('tokenExpiryTime'))
            : null,
        refreshDataSegmentList: false,
    },
    getters: {
        loading(state) {
            return state.loading
        },
        error(state) {
            return state.error
        },
        selectedItem(state) {
            return state.selectedItem
        },
        success(state) {
            return state.success
        },
        redirectTo(state) {
            return state.redirectTo
        },
        data(state) {
            return state.data
        },
        reload(state) {
            return state.reload
        },
        hidePopup(state) {
            return state.hidePopup
        },
        showPageLoader(state) {
            return state.showPageLoader
        },
        onSubmit(state) {
            return state.onSubmit
        },
        redirectUrl(state) {
            return state.redirectUrl
        },
        kycRole(state) {
            return state.kycRole
        },
        changeDetect(state) {
            return state.changeDetect
        },
        displaySettings(state) {
            return state.displaySettings
        },
        fileLoading(state) {
            return state.fileLoading
        },
        tokenExpiryTime(state) {
            return state.tokenExpiryTime
        },
        refreshDataSegmentList(state) {
            return state.refreshDataSegmentList
        },
    },
    mutations: {
        setLoading(state, payload) {
            state.loading = payload
        },
        setError(state, payload) {
            state.error = payload
        },
        clearError(state) {
            state.error = null
        },
        clearItem(state) {
            state.selectedItem = null
        },
        setItem(state, payload) {
            state.selectedItem = payload
        },
        setSuccess(state, payload) {
            state.success = payload
        },
        clearSuccess(state) {
            state.success = null
        },
        redirectTo(state, payload) {
            state.redirectTo = payload
        },
        setData(state, payload) {
            state.data = payload
        },
        pushData(state, data) {
            state.data.push(data)
        },
        unshiftData(state, data) {
            state.data.unshift(data)
        },
        reloadList(state, payload) {
            state.reload = payload
        },
        hidePopup(state, payload) {
            state.hidePopup = payload
        },
        showPageLoader(state, payload) {
            state.showPageLoader = payload
        },
        onSubmit(state, payload) {
            state.onSubmit = payload
        },
        setRedirectUrl(state, payload) {
            state.redirectUrl = payload
        },
        setKycRole(state, payload) {
            state.kycRole = payload
        },
        setChangeDetect(state, payload) {
            state.changeDetect = payload
        },
        setDisplaySetting(state, data) {
            localStorage.setItem('displaySettings', JSON.stringify(data))
            state.displaySettings = data
        },
        setFileLoading(state, payload) {
            state.fileLoading = payload
        },
        setTokenExpiryTime(state, payload) {
            localStorage.setItem('tokenExpiryTime', JSON.stringify(payload))
            state.tokenExpiryTime = payload
        },
        setRefreshDataSegmentList(state, payload) {
            state.refreshDataSegmentList = payload
        },
    },
    actions: {
        reloadList({ commit }, payload) {
            commit('reloadList', payload)
        },
        setData({ commit }, payload) {
            commit('setData', payload)
        },
        hidePopup({ commit }, payload) {
            commit('hidePopup', payload)
        },
        showPageLoader({ commit }, payload) {
            commit('showPageLoader', payload)
        },
        onSubmit({ commit }, payload) {
            commit('onSubmit', payload)
        },
        setRedirectUrl({ commit }, payload) {
            commit('setRedirectUrl', payload)
        },
        setKycRole({ commit }, payload) {
            commit('setKycRole', payload)
        },
    },
}
