export default {
    form: (state) => state.form,
    getField: (state) => {
        return (key) => {
            return _.get(state.form, key)
        }
    },
}
