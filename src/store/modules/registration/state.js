export default {
    form: {},
    useOcr: false,
    restrictDuplicateCustomers: false,
    isIndustrial: true,
    isTaxIdRequired: false,
    isTaxIdMandatory: false,
    taxIdNote: null,
    idDocumentExpiryNote: null,
}
