export default {
    customersData: {
        customers: {
            fields: [
                {
                    key: 'profile',
                    label: '',
                    class: 'profile-absolute',
                },
                {
                    key: 'customer',
                    label: 'CUSTOMER',
                    sortable: true,
                },
                {
                    key: 'full_name',
                    label: 'FULL NAME',
                    sortable: true,
                },
                {
                    key: 'nationality',
                    label: 'NATIONALITY',
                    sortable: true,
                },
                {
                    key: 'residence',
                    label: 'RESIDENCE',
                    sortable: true,
                },
                {
                    key: 'registration_date',
                    label: 'Registration Date',
                    sortable: true,
                },
                {
                    key: 'status',
                    label: 'Status',
                    sortable: true,
                    class: 'text-center status-absolute-pos',
                },
                {
                    key: 'action',
                    label: 'ACTION',
                },
            ],
            items: [
                {
                    profile: 'member-1.png',
                    customer: '296192',
                    full_name: 'Chineze Afamefuna',
                    nationality: 'Indian',
                    residence: 'United Kingdom',
                    registration_date: 'Jun 10, 2019 6:35 p.m.',
                    status: [
                        {
                            cssClass: 'pending',
                            icon: 'icon-loading',
                            label: 'Pending',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
                {
                    profile: 'member-2.png',
                    customer: '221021',
                    full_name: 'Alex Edwards',
                    nationality: 'Pakistani',
                    residence: 'Pakistan',
                    registration_date: 'Jun 10, 2019 10:30 p.m.',
                    status: [
                        {
                            cssClass: 'approved',
                            icon: 'icon-check',
                            label: 'Approved',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
                {
                    profile: 'member-3.png',
                    customer: '236912',
                    full_name: 'Regina Pollastro',
                    nationality: 'British',
                    residence: 'United Kingdom',
                    registration_date: 'Jun 9, 2019 10:45 a.m.',
                    status: [
                        {
                            cssClass: 'rejected',
                            icon: 'icon-wrong',
                            label: 'Rejected',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
                {
                    profile: 'member-4.png',
                    customer: '266171',
                    full_name: 'Boris Ukhtomsky',
                    nationality: 'American',
                    residence: 'United Kingdom',
                    registration_date: 'Jun 9, 2019 9:30 p.m.',
                    status: [
                        {
                            cssClass: 'approved',
                            icon: 'icon-check',
                            label: 'Approved',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
                {
                    profile: 'member-5.png',
                    customer: '284619',
                    full_name: 'Kimmy McIlmorie',
                    nationality: 'Algerian',
                    residence: 'United Kingdom',
                    registration_date: 'Jun 9, 2019 9:10 p.m.',
                    status: [
                        {
                            cssClass: 'approved',
                            icon: 'icon-check',
                            label: 'Approved',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
                {
                    profile: 'member-6.png',
                    customer: '299488',
                    full_name: 'Noell Blue',
                    nationality: 'Belgian',
                    residence: 'United Kingdom',
                    registration_date: 'Jun 8, 2019 6:45 p.m.',
                    status: [
                        {
                            cssClass: 'approved',
                            icon: 'icon-check',
                            label: 'Approved',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
                {
                    profile: 'member-7.png',
                    customer: '297438',
                    full_name: 'Qin Shi',
                    nationality: 'Colombian',
                    residence: 'United Kingdom',
                    registration_date: 'Jun 8, 2019 4:24 p.m.',
                    status: [
                        {
                            cssClass: 'rejected',
                            icon: 'icon-wrong',
                            label: 'Rejected',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
                {
                    profile: 'member-8.png',
                    customer: '275731',
                    full_name: 'Julian Gruber',
                    nationality: 'British',
                    residence: 'Pakistan',
                    registration_date: 'Jun 8, 2019 3:10 p.m.',
                    status: [
                        {
                            cssClass: 'pending',
                            icon: 'icon-loading',
                            label: 'Pending',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
                {
                    profile: 'member-9.png',
                    customer: '217831',
                    full_name: 'Mathijn Agter',
                    nationality: 'Indian',
                    residence: 'Indian',
                    registration_date: 'Jun 8, 2019 11:32 p.m.',
                    status: [
                        {
                            cssClass: 'approved',
                            icon: 'icon-check',
                            label: 'Approved',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
                {
                    profile: 'member-10.png',
                    customer: '293397',
                    full_name: 'Shadrias Pearson',
                    nationality: 'Pakistani',
                    residence: 'Pakistan',
                    registration_date: 'Jun 7, 2019 5:45 p.m.',
                    status: [
                        {
                            cssClass: 'approved',
                            icon: 'icon-check',
                            label: 'Approved',
                        },
                    ],
                    action: [
                        {
                            text: 'View Profile',
                            icon: 'icon-eye-view',
                        },
                    ],
                },
            ],
        },
    },
    customerRecord: localStorage.getItem('customerRecord')
        ? JSON.parse(localStorage.getItem('customerRecord'))
        : {},
    refreshProfile: false,
    advanceSearchFields: {
        idenfoEngine: [
            {
                label: 'first Name',
                type: 'text-field',
                placeholder: 'Search by first name',
                isRequired: false,
                isLocked: true,
                key: 'basicInfo.firstName',
            },
            {
                label: 'middle Name',
                type: 'text-field',
                placeholder: 'Search by middle name',
                isRequired: false,
                isLocked: true,

                key: 'basicInfo.middleName',
            },
            {
                label: 'last Name',
                type: 'text-field',
                placeholder: 'Search by last name',
                isRequired: false,
                isLocked: true,

                key: 'basicInfo.lastName',
            },
            {
                label: 'gender',
                type: 'list',
                association: 'genders',
                placeholder: 'Select Gender',
                isRequired: false,
                isLocked: true,

                key: 'basicInfo.genderId',
            },
            {
                label: 'nationality',
                type: 'list',
                association: 'nationalities',
                placeholder: 'Select Nationality',
                isRequired: false,
                isLocked: true,

                key: 'basicInfo.nationalityId',
            },
            {
                label: 'country Of Residence',
                type: 'list',
                association: 'countries',
                placeholder: 'Select Country Of Residence',
                isRequired: false,
                isLocked: true,

                key: 'basicInfo.countryOfResidenceId',
            },
            {
                label: 'work Type',
                type: 'list',
                association: 'work-types',
                placeholder: 'Select Work Type',
                isRequired: false,
                isLocked: true,

                key: 'occupationInfo.workTypeId',
            },
            {
                label: 'industry',
                type: 'list',
                association: 'industries',
                placeholder: 'Select Industry',
                isRequired: false,
                isLocked: true,

                key: 'occupationInfo.industryId',
            },
        ],
        kyc: [
            {
                label: 'KYC Status',
                type: 'dropdown',
                key: 'status',
                placeholder: 'Select KYC Status',
                options: [
                    { text: 'Approved', value: 'approved' },
                    { text: 'Pending', value: 'pending' },
                    {
                        text: 'Rejected',
                        value: 'rejected',
                    },
                ],
                isRequired: false,
                _id: 'status',
            },
            {
                label: 'name screening hit type',
                type: 'dropdown',
                placeholder: 'Select Name Screening Hit Type',
                options: [
                    { text: 'PEP hit', value: 'pepHit' },
                    { text: 'Enforcement hit', value: 'enforcementHit' },
                    { text: 'Blacklist hit', value: 'blackListHit' },
                    { text: 'Sanction hit', value: 'sanctionHit' },
                ],
                isRequired: false,
                _id: 'nameScreening',
            },
            {
                label: 'Risk Rating Level',
                type: 'dropdown',
                placeholder: 'Select Risk Rating Level',
                options: [
                    {
                        text: 'High Risk',
                        value: 'high',
                    },
                    { text: 'Medium Risk', value: 'medium' },
                    { text: 'Low Risk', value: 'low' },
                    { text: 'Sanctioned', value: 'sanction' },
                ],
                isRequired: false,
                _id: 'riskRating',
            },
            // {
            //     label: 'Registration Channel',
            //     type: 'radio',
            //     key: 'registrationChannelId',
            //     options: [],
            //     isRequired: false,
            // },
            {
                label: 'ID Type',
                type: 'dropdown',
                placeholder: 'Select ID Type',
                options: [
                    { value: 'passport', text: 'Passport' },
                    { value: 'lisence', text: 'Driving Licence' },
                    { value: 'id', text: 'National ID card' },
                ],
                isRequired: false,
                _id: 'type',
            },
            {
                label: 'Product Type',
                type: 'list',
                association: 'products',
                key: 'accountInfo.productId',
                placeholder: 'Select Product Type',
                isRequired: false,
                _id: 'productId',
            },
        ],
    },
}
