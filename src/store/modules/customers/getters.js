export default {
    customersData: (state) => state.customersData,
    customerRecord: (state) => state.customerRecord,
    refreshProfile: (state) => state.refreshProfile,
    advanceSearchFields: (state) => state.advanceSearchFields,
}
