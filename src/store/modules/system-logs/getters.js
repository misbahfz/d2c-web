export default {
    systemLogsData: (state) => state.systemLogsData,
    systemLogFields: (state) => state.systemLogFields,
    systemLogModules: (state) => state.systemLogModules,
    systemLogActions: (state) => state.systemLogActions,
    totalRecords: (state) => state.totalRecords,
}
