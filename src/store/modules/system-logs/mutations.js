export default {
    setSystemLogsData(state, payload) {
        state.systemLogsData = payload
    },
    setTotalRecords(state, payload) {
        state.totalRecords = payload
    },
}
