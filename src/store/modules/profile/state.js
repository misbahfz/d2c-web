export default {
    profile: {
        screenCustomer: {
            customerDetails: [],

            fields: [
                {
                    key: 'full_name',
                    label: 'FULL NAME',
                    sortable: true,
                },
                {
                    key: 'country',
                    label: 'COUNTRY',
                    sortable: true,
                },
                {
                    key: 'associates',
                    label: 'Associates',
                    sortable: true,
                },
                {
                    key: 'sanction',
                    label: 'SANCTION',
                    thClass: 'sanction-icon',
                    class: 'text-center',
                },
                {
                    key: 'rep',
                    label: 'PEP',
                    thClass: 'rep-icon',
                    class: 'text-center',
                },
                {
                    key: 'enforcement',
                    label: 'Enforcement',
                    thClass: 'enforcement-icon',
                    class: 'text-center',
                },
                {
                    key: 'blackList',
                    label: 'Client Black List',
                    thClass: 'blacklist-icon',
                    class: 'text-center',
                },
                {
                    key: 'hit',
                    label: 'HIT determination',
                    sortable: true,
                    class: 'mobile-text-center',
                },
                {
                    key: 'genuine',
                    label: 'GENUINE',
                    sortable: false,
                    class: 'text-center mobile-text-center',
                },
                {
                    key: 'not-genuine',
                    label: 'Not Genuine',
                    sortable: false,
                    class: 'text-center mobile-text-center',
                },
            ],
            items: [],
        },
        kycData: {
            fields: [
                {
                    key: 'factor',
                    label: 'RISK FACTOR',
                    sortable: true,
                    class: 'risk-factor',
                },
                {
                    key: 'number',
                    label: 'SCORE',
                    class: 'text-center risk-score',
                    sortable: true,
                },
                {
                    key: 'level',
                    label: 'Risk Level',
                    class: 'text-center risk-level',
                    thClass: 'w-320px w-270-lg',
                    sortable: true,
                },
            ],

            overrideFields: [
                {
                    key: 'factor',
                    label: 'RISK FACTOR OVERRIDE',
                    sortable: true,
                    class: 'risk-factor',
                },
                {
                    key: 'overRideTo',
                    label: 'Override To',
                    sortable: true,
                    class: 'risk-score',
                    thClass: 'w-150px w-130-lg',
                    formatter: (value) => {
                        return _.startCase(value)
                    },
                },
                {
                    key: 'level',
                    label: 'Risk Level',
                    class: 'text-center risk-level',
                    sortable: true,
                },
            ],
        },

        customerInformation: {
            customerDetails: [],

            tags: [
                {
                    description: [
                        {
                            headings: 'Sanction Hit',
                            icon: 'icon-sanction',
                            actions: 'Rejected',
                        },
                        {
                            headings: 'PEP Hit',
                            icon: 'icon-tomb',
                            actions: 'Rejected',
                        },
                    ],
                },
                {
                    description: [
                        {
                            headings: 'Enforcement Hit',
                            icon: 'icon-enforcement',
                            actions: 'Active',
                        },
                        {
                            headings: 'Client Black List Hit',
                            icon: 'icon-blacklist',
                            actions: 'Active',
                        },
                    ],
                },
            ],

            tagsList: [
                {
                    statusHeading: 'Documents Verification',
                    description: [
                        {
                            headings: 'Document Matched',
                            icon: 'icon-documentation',
                            actions: 'Active',
                        },
                    ],
                },
                {
                    statusHeading: 'Risk Rating',
                    description: [
                        {
                            headings: 'Sanctioned',
                            icon: 'icon-graph',
                            actions: 'Rejected',
                        },
                    ],
                },
            ],
        },

        activityTimeline: {
            customerDetails: [
                {
                    heading: 'Customer Information',
                    descriptions: [
                        {
                            title: 'Idenfo ID:',
                            detail: '210345',
                        },
                        {
                            title: 'Bank Customer ID:',
                            detail: 'N/A',
                        },
                        {
                            title: 'First Name:',
                            detail: 'Henk',
                        },
                        {
                            title: 'Middle Name:',
                            detail: 'John',
                        },
                        {
                            title: 'Last Name:',
                            detail: 'Fortuin',
                        },
                        {
                            title: 'Gender:',
                            detail: 'Male',
                        },
                        {
                            title: 'Date of Birth:',
                            detail: 'June 24, 1984',
                        },
                        {
                            title: 'Country of Residence:',
                            detail: 'United Kingdom',
                        },
                        {
                            title: 'Nationality:',
                            detail: 'American',
                        },

                        {
                            title: 'Record Last Updated:',
                            detail: 'June 19,2019',
                        },
                    ],
                },
            ],

            yearDetails: [
                {
                    days: 'Today',
                    describe: [
                        {
                            image: 'member-1.png',
                            name: 'Jacqueline Asong',
                            subHeading: 'Approver',
                            date: 'July 4, 2019 10:45 a.m.',
                            details:
                                'Submitted KYC Review Status and changed KYC Status to <b> Approved</b> with the following comment, <span>"I didn\'t found anything unusual with the profile. All records are clear and up to date." </span>',
                            fileName: true,
                        },
                        {
                            image: 'member-1.png',
                            name: 'Jacqueline Asong',
                            subHeading: 'Approver',
                            date: 'July 4, 2019 10:15 a.m.',
                            details: 'Initiated KYC Review process related to,',
                            blockTags: {
                                headings: 'Sanctioned',
                                icon: 'icon-graph',
                                actions: 'Rejected',
                            },
                        },
                    ],
                },
                {
                    days: 'June 3, 2019',
                    describe: [
                        {
                            image: 'idenfo.png',
                            name: 'IDENFO',
                            subHeading: 'System Activity',
                            date: 'July 3, 2019 12:10 p.m.',
                            details:
                                'Generated system alert related to following factor,',
                            blockTags: {
                                headings: 'Sanctioned',
                                icon: 'icon-graph',
                                actions: 'Rejected',
                            },
                        },
                    ],
                },
                {
                    days: 'June 1, 2019',
                    describe: [
                        {
                            image: 'idenfo.png',
                            name: 'IDENFO',
                            subHeading: 'System Activity',
                            date: 'July 1, 2019 11:10 a.m.',
                            details:
                                'Completed liveness detection process with the Matched result.',
                        },
                        {
                            image: 'idenfo.png',
                            name: 'IDENFO',
                            subHeading: 'System Activity',
                            date: 'July 1, 2019 11:10 a.m.',
                            details:
                                'Completed liveness detection process with the Matched result.',
                        },
                        {
                            image: 'member-5.png',
                            name: 'Cvita Doleschall',
                            subHeading: 'Relationship Manager',
                            date: 'July 1, 2019 11:05 a.m.',
                            details:
                                'Registered new customer in to the system and uploaded required documents. ',
                        },
                    ],
                },
            ],
        },
    },
}
