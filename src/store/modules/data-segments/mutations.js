export default {
    setDataSegmentList(state, data) {
        state.dataSegmentList = data
    },
    setDataSegmentId(state, data) {
        localStorage.setItem('activeDataSegmentId', data)
        state.dataSegmentId = data
    },
}
