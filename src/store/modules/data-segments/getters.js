export default {
    dataSegmentList: function(state) {
        return state.dataSegmentList
    },
    dataSegmentFields: function(state) {
        return state.dataSegmentFields
    },
    dataSegmentId(state) {
        return state.dataSegmentId
            ? state.dataSegmentId
            : localStorage.getItem('activeDataSegmentId')
    },
    nationalities(state) {
        return state.nationalities
    },
}
