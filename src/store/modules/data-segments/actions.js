const apiUrl = process.env.VUE_APP_API_URL
import axios from 'axios'
const headers = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('vueauth_token')}`,
    },
}
export default {
    async addDataSegment({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.post(
                apiUrl + 'users/data-segments',
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
            return data
        } catch (errors) {
            console.log(Object.keys(errors.response.data.errors)[0])
            commit('setLoading', false)
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
    },
    async updateDataSegment({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.put(
                apiUrl + 'access-control/data-segments/' + payload.id,
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
        } catch (errors) {
            commit('setLoading', false)
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
    },
    async getAllDataSegments({ commit }, payload) {
        try {
            await axios.get(apiUrl + 'access-control/data-segments/', {
                params: payload,
                headers: {
                    Authorization: `Bearer ${localStorage.getItem(
                        'vueauth_token'
                    )}`,
                },
            })
            // commit('setDataSegmentList', data)
        } catch (errors) {
            console.log(errors)
            commit('setLoading', false)
            commit('setError', errors.response.data.message)
        }
    },
}
