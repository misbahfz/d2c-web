const apiUrl = process.env.VUE_APP_API_URL
import axios from 'axios'
const headers = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('vueauth_token')}`,
    },
}
export default {
    // update Onboarding Customer action
    async updateOnboardingConfiguration({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.put(
                apiUrl + 'configurations/ocr/' + payload._id,
                payload,
                headers
            )
            commit('setLoading', true)
            commit('setSuccess', data.message)
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors.response)
        }
    }, // End of updateOnboardingConfiguration()

    async updateConfigBranding({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.put(
                apiUrl + 'configurations/branding/' + payload._id,
                payload,
                headers
            )
            commit('setLoading', true)
            commit('setSuccess', data.message)
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors.response)
        }
    },
}
