export default {
    rolesList: function(state) {
        return state.rolesList
    },
    roleFields: function(state) {
        return state.roleFields
    },
    permissions: function(state) {
        return state.permissions
    },
    statusReview: function(state) {
        return state.statusReview
    },
}
