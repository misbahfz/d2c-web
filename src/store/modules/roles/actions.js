const apiUrl = process.env.VUE_APP_API_URL
import axios from 'axios'
const headers = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('vueauth_token')}`,
    },
}
export default {
    async addRole({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.post(
                apiUrl + 'access-control/roles',
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
        } catch (errors) {
            commit('setLoading', false)
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
    },
    async updateRole({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.put(
                apiUrl + 'access-control/roles/' + payload.id,
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
        } catch (errors) {
            commit('setLoading', false)
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
    },
    async getAllRoles({ commit }, payload) {
        try {
            await axios.get(apiUrl + 'access-control/roles', {
                params: payload,
                headers: {
                    Authorization: `Bearer ${localStorage.getItem(
                        'vueauth_token'
                    )}`,
                },
            })
            // await commit('setRolesList', data)
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors)
        }
    },
}
