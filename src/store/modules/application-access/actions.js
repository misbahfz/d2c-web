import axios from 'axios'
const apiUrl = process.env.VUE_APP_API_URL
const headers = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('vueauth_token')}`,
    },
}

export default {
    async addApplicationAccess({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.post(
                apiUrl + 'access-control/api-access',
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
        } catch (errors) {
            commit('setLoading', false)
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
    },
    async updateApplicationAccess({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.put(
                apiUrl + 'access-control/api-access/' + payload.id,
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
        } catch (errors) {
            commit('setLoading', false)
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
    },
    async getAll({ commit }, payload) {
        try {
            let { data } = await axios.get(
                apiUrl + 'access-control/api-access',
                {
                    params: payload,
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'vueauth_token'
                        )}`,
                    },
                }
            )
            await commit('setApplicationAccessList', data)
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors)
        }
    },
}
