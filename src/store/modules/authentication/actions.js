const apiUrl = process.env.VUE_APP_API_URL
import axios from 'axios'
import { loadStripe } from '@stripe/stripe-js'
const stripePromise = loadStripe(
    'pk_test_51Io5W0HoDw6DABRY2acOk2AlZVNSlwXhXN4ndtsOAP6seuQCahoVlg5TC2BNXbmFKwYUlO5WePbtMSel2RzLHnUq00ZYcFKf63'
) //loadStripe(process.env.STRIPE_PK)

export default {
    async contactUsForm({ commit }, payLoad) {
        commit('setLoading', true)
        try {
            let { data } = await axios.post(apiUrl + 'users/support', payLoad)
            commit('setSuccess', data.message)
            commit('setLoading', false)
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors.response.data.message)
        }
    },

    authLogin({ commit }, payLoad) {
        commit('setLoading', true)
        // try {
        //     commit('setLoading', true)
        //     let { data } = await axios.post(
        //         apiUrl + 'users/login/',
        //         payLoad
        //     )
        //     commit('setSuccess', data.message)
        //     commit('setToken', data.data.accessToken)
        //     commit('setUser', data.data.profile)
        //     commit('setLoading', false)
        //     return true
        // } catch (errors) {
        //     commit('setLoading', false)
        //     commit('setError', errors.response.data.message)
        // }
        try {
            httpRequest['$auth']
                .login(payLoad)
                .then((response) => {
                    commit('setSuccess', response.data.message)
                    commit('setToken', response.data.data.accessToken)
                    commit('setUser', response.data.data.profile)
                    commit('setLoading', false)
                })
                .catch(function onError(error) {
                    commit('setLoading', false)
                    commit('setError', error.response.data.message)
                })
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors.response.data.message)
        }
    },
    async stripeCustomerPortalRedirect({ commit }, payLoad) {
        try {
            let { data } = await axios.post(apiUrl + 'users/billing', payLoad)
            commit('setSuccess', data.message)
            commit('setLoading', false)
            window.location.href = data.data.session
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors.response.data.message)
        }
    },
    async createCheckoutSession({ commit }, payLoad) {
        try {
            let { data } = await axios.post(
                apiUrl + 'users/create-checkout-session',
                payLoad
            )
            const stripe = await stripePromise

            const result = await stripe.redirectToCheckout({
                sessionId: data.sessionId,
            })

            if (result.error) {
                // If `redirectToCheckout` fails due to a browser or network
                // error, display the localized error message to your customer
                // using `result.error.message`.
            }
            commit('setSuccess', data.message)
            commit('setLoading', false)
        } catch (errors) {
            commit('setLoading', false)
            //            commit('setError', errors.response)
            console.log(errors)
        }
    },
    async authLogout({ commit }) {
        try {
            let { data } = await axios.post(apiUrl + 'users/logout')
            commit('setUser', null)
            commit('setToken', null)
            commit('setSuccess', data.message)
            commit('setDataSegmentId', null)
            localStorage.removeItem('dataSegmentId')
            localStorage.removeItem('activeDataSegmentId')

            // httpRequest.$auth
            //     .logout(payLoad)
            //     .then((response) => {
            //         commit('setUser', null)
            //         commit('setToken', null)
            //         commit('setSuccess', 'You have logged out successfully.')
            //     })
            //     .catch(function onError(error) {
            //         commit('setUser', null)
            //         commit('setToken', null)
            //         commit('setError', error.message)
            //     })
        } catch (error) {
            commit('setUser', null)
            commit('setToken', null)
            commit('setError', error.response.data.message)
        }
    },

    async authChangePassword({ commit }, payLoad) {
        try {
            commit('setLoading', true)
            let { data } = await axios.post(
                apiUrl + 'users/password/change',
                payLoad,
                {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'vueauth_token'
                        )}`,
                    },
                }
            )
            commit('setSuccess', data.message)
            commit('setLoading', false)
            return true
        } catch ({ response }) {
            let { data } = response
            let message = 'There is someting wrong'
            if (data.errors && Object.keys(data.errors).length) {
                message = data.errors[Object.keys(data.errors)[0]]
            } else if (data.message) {
                message = data.message
            }

            commit('setLoading', false)
            commit('setError', message)
            return false
        }
    },

    async authForgotPassword({ commit }, payLoad) {
        try {
            commit('setLoading', true)
            let { data } = await axios.post(
                apiUrl + 'users/password/reset',
                payLoad
            )
            commit('setSuccess', data.message)
            commit('setData', data.message)
            commit('setLoading', false)
            return true
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors.response.data.message)
        }
    },

    async authResetPassword({ commit }, payLoad) {
        try {
            commit('setLoading', true)
            let { data } = await axios.post(
                apiUrl + 'users/password/set/' + payLoad.token,
                payLoad
            )
            commit('setSuccess', data.message)
            commit('setLoading', false)
            return true
        } catch ({ response }) {
            let { data } = response
            let message = 'There is someting wrong'
            if (data.errors && Object.keys(data.errors).length) {
                message = data.errors[Object.keys(data.errors)[0]]
            } else if (data.message) {
                message = data.message
            }

            commit('setLoading', false)
            commit('setError', message)
            return false
        }
    },

    async authCreatePassword({ commit }, payLoad) {
        try {
            commit('setLoading', true)
            let { data } = await axios.post(
                apiUrl + 'users/activate-account/' + payLoad.token,
                payLoad
            )
            commit('setToken', data.data.accessToken)
            commit('setUser', data.data.profile)
            commit('setSuccess', data.message)
            commit('setLoading', false)
            return true
        } catch ({ response }) {
            let { data } = response
            let message = 'There is someting wrong'
            if (data.errors && Object.keys(data.errors).length) {
                message = data.errors[Object.keys(data.errors)[0]]
            } else if (data.message) {
                message = data.message
            }

            commit('setLoading', false)
            commit('setError', message)
            return false
        }
    },
}
