export default {
    setUser(state, data) {
        localStorage.setItem('user', JSON.stringify(data))
        state.user = data
    },

    setToken(state, data) {
        localStorage.setItem('vueauth_token', data)
        state.vueauth_token = data
    },

    setScopes(state, data) {
        state.scopes = data
    },

    setUserId(state, data) {
        state.userId = data
    },
}
