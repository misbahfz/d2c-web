export default {
    user: localStorage.getItem('user')
        ? JSON.parse(localStorage.getItem('user'))
        : false,
    vueauth_token: localStorage.getItem('vueauth_token')
        ? localStorage.getItem('vueauth_token')
        : false,
    userId: null,
    scopes: [],
}
