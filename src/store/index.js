import Vue from 'vue'
import Vuex from 'vuex'
import shared from './modules/shared'
import dashboard from './modules/dashboard/'
import customers from './modules/customers/'
import systemsUsers from './modules/system-users/'
import systemLogs from './modules/system-logs/'
import configuration from './modules/configuration/'
import profile from './modules/profile/'
import authentication from './modules/authentication/'
import applicationAccess from './modules/application-access/'
import roles from './modules/roles/'
import dataSegments from './modules/data-segments/'
import formBuilder from './modules/form-builder/'
import registration from './modules/registration/'
import alerts from './modules/alerts/'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        shared,
        dashboard,
        customers,
        systemsUsers,
        systemLogs,
        configuration,
        profile,
        authentication,
        applicationAccess,
        roles,
        dataSegments,
        formBuilder,
        registration,
        alerts,
    },
    strict: debug,
})
