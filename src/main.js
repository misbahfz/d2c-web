import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from '@/store/index'
import BootstrapVue from 'bootstrap-vue'
import vbclass from 'vue-body-class'
import Toasted from 'vue-toasted'
import VeeValidate from 'vee-validate'
import VueAxios from 'vue-axios'
import VueAuthenticate from 'vue-authenticate'
import axios from 'axios'
import {
    VueAuthenticateConfig,
    VeeValidateConfig,
    ToastedConfig,
} from '@/config'

// Load the full build.
window._ = require('lodash')

Vue.use(require('vue-moment'))
Vue.use(VueAxios, axios)
Vue.use(VueAuthenticate, VueAuthenticateConfig)
Vue.use(VeeValidate, VeeValidateConfig)
Vue.use(Toasted, ToastedConfig)
Vue.use(BootstrapVue)
Vue.use(vbclass, router)

Vue.config.productionTip = false

require('@/auto-import.js')
require('@/route-middleware')
require('@/interceptor')
require('./filters')
require('./directives/authenticated-image')

window.httpRequest = Vue.prototype

new Vue({
    router: router,
    store,
    watch: {
        $route: function() {
            this.checkMeta()
            this.pageTop()
        },
    },
    methods: {
        checkMeta() {
            document.title =
                process.env.VUE_APP_TITLE + ' | ' + this.$route.meta.title
        },
        pageTop() {
            window.scrollTo(0, 0)
        },
    },
    render: (h) => h(App),
}).$mount('#app')
