const baseUrl = process.env.VUE_APP_API_URL || 'http://localhost:8080/api/'

export const VueAuthenticateConfig = {
    tokenName: 'token',
    storageNamespace: '',
    baseUrl: baseUrl,
    storageType: 'localStorage',
    logoutUrl: 'users/logout',
    loginUrl: 'users/login',
    providers: {
        // Define OAuth providers config
        oauth2: {
            name: 'oauth2',
            url: 'Token/Exchange',
        },
    },
}

export const VeeValidateConfig = {
    inject: true,
    /* Avoid Field attribute of vee-valifate to conflict with vue's bootstrap table Field attribute */
    fieldsBagName: 'veeFields',
    /* Vee-Validate occur on event triggers */
    events: '',
}

export const ToastedConfig = {
    singleton: true,
    position: 'bottom-right',
    theme: 'bubble',
    duration: 3000,
    iconPack: 'material',
}
