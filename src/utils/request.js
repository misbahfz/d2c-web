import Vue from 'vue'
import axios from 'axios'
import store from '@/store/index'

// Create axios instance
const service = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
    timeout: 300000, // Request timeout
})

// Request intercepter
service.interceptors.request.use(
    (config) => {
        const token = Vue.prototype.$auth.getToken()
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token // Set JWT token
        }

        return config
    },
    (error) => {
        // Do something with request error
        console.log(error) // for debug
        Promise.reject(error)
    }
)

// response pre-processing
service.interceptors.response.use(
    (response) => {
        if (response.headers.authorization) {
            Vue.prototype.$auth.setToken(response.headers.authorization)
            response.data.token = response.headers.authorization
        }

        return response
    },
    ({ response }) => {
        let message = 'There is someting wrong'
        if (response) {
            if (response.status == 401) {
                localStorage.removeItem('dataSegmentId')
                localStorage.removeItem('activeDataSegmentId')
                store.commit('setDataSegmentId', null)
                store.dispatch('authLogout')
            }
            if (response.data) {
                let data = response.data
                if (data) {
                    if (data.errors && Object.keys(data.errors).length) {
                        message = data.errors[Object.keys(data.errors)[0]]
                    } else if (data.error) {
                        message = data.error
                    } else if (data.message) {
                        message = data.message
                    }
                }
            }
            if (response.error && response.error.message) {
                message = response.error.message
            }
        }

        return Promise.reject(message)
    }
)

export default service
