import router from './router'
import store from '@/store/index'
import jwt from '@/jwt'
let entryUrl = null

router.beforeEach(async (to, from, next) => {
    let verify = await jwt.verify()

    let user
    if (store.getters.user && verify) {
        user = store.getters.user
    }
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        /**
         * If the route requires a Auth User
         */
        if (!user) {
            /**
             * If the user is unauthenticated
             * redirect to login
             */
            entryUrl = to.path
            store.commit('setRedirectUrl', entryUrl)
            next({ name: 'login-page' })
        } else {
            //Check routes permission
            if (
                to.meta.permission &&
                to.meta.permission.length &&
                !!_.intersection(to.meta.permission, store.getters['scopes'])
                    .length
            ) {
                next()
            } else {
                if (to.meta.permission && to.meta.permission.length == 0) {
                    next()
                } else {
                    if (store.getters['scopes'].length) {
                        next({ name: store.getters['scopes'][0] })
                    } else {
                        next(new Error('403 Forbidden'))
                    }
                }
            }
        }
    } else if (to.matched.some((record) => record.meta.requiresGuest)) {
        /**
         * If the route requires a Guest User
         */
        if (user) {
            /**
             * If the user is authenticated
             * redirect to dashboard
             */
            next({ name: 'view-activity-dashboard' })
        } else {
            /**
             * Complete the user request normally
             */
            next()
        }
    } else {
        /**
         * Complete the user request normally
         */
        next()
    }
})

export default router
