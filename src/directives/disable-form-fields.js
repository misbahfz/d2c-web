import Vue from 'vue'
import store from '@/store/index'

Vue.directive('disable-form-fields', function(form, binding) {
    form.addEventListener('change', function() {
        disableSubmitButton(form)
    })

    form.addEventListener('submit', function() {
        disableFields(form)
    })
})

function disableSubmitButton(form) {
    let submit = form.querySelector('[type="submit"]')
    submit.disabled = true
}

function disableFields(form) {
    // disable submit button
    let submit = form.querySelector('[type="submit"]')
    submit.disabled = true

    // disable all inputs
    let inputs = form.querySelectorAll('input, select, textarea')
    inputs.forEach(function(element) {
        if (element.tagName === 'SELECT' || element.type === 'range') {
            createHiddenInput(element, form)
            element.disabled = true
        } else if (element.type === 'radio' || element.type === 'checkbox') {
            if (element.checked) {
                createHiddenInput(element, form)
            }
            element.disabled = true
        } else {
            element.readOnly = true
        }
    })
}

function createHiddenInput(element, form) {
    // create a hidden input
    var input = document.createElement('input')
    input.setAttribute('type', 'hidden')
    input.setAttribute('name', element.name)
    input.setAttribute('value', element.value)
    // append input to the form.
    form.appendChild(input)
}
